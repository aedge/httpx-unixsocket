# httpx-unixoscket

Unix Domain Socket (UDS) support for HTTPX.

This is heavily based on https://github.com/florimondmanca/httpx-unixsocket-poc, albeit without the “no maintenance intended” clause ;-) Thank you for your amazing work, [Florimond](https://github.com/florimondmanca)!


## Installation

(just install from git right now)

<!--**Note:** the API will be unstable for the next couple of iterations, probably a good idea would be to freeze to a specific version, e. g.:

```bash
poetry add httpx-unixsocket@=0.1.0
```

or in your requirements.txt:

```
httpx-unixsocket==0.1.0
```
-->

## Usage

This package provides a custom connection pool that connects to hosts via UDS instead of a regular TCP Internet socket.

As a real-world example, let's request the Docker API version through the Docker socket...

_**Hint**: try this code in IPython, or with Python 3.8+ with `python -m asyncio`._

```python
>>> import httpx
>>> from httpx_unixsocket import UnixSocketConnectionPool
>>> dispatch = UnixSocketConnectionPool(uds="/var/run/docker.sock")
>>> async with httpx.AsyncClient(dispatch=dispatch) as client:
...     # This request will connect to the Docker API through the socket file.
...     response = await client.get("http://localhost/version")
...
>>> response.json()["Version"]
'19.03.2'
```

(If you get a `FileNotFoundError`, make sure that the Docker daemon is running.)


## Development notes

```bash
poetry install
poetry run pytest
```


## License

MIT
