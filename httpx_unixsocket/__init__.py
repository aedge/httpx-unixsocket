import asyncio
import functools
import ssl
import typing

import sniffio
from httpcore._backends.asyncio import SocketStream, AsyncioBackend
from httpcore._types import TimeoutDict
from httpcore._exceptions import ConnectTimeout
from httpcore._async.connection import AsyncHTTPConnection
from httpcore._utils import url_to_origin


async def open_uds_socket_stream(
    path: str,
    hostname: str,
    timeout: TimeoutDict,
    ssl_context: typing.Optional[ssl.SSLContext],
) -> SocketStream:
    assert sniffio.current_async_library() == "asyncio"

    server_hostname = hostname if ssl_context else None

    try:
        stream_reader, stream_writer = await asyncio.wait_for(
            asyncio.open_unix_connection(
                path, ssl=ssl_context, server_hostname=server_hostname
            ),
            timeout.get("connect_timeout", 10),
        )
    except asyncio.TimeoutError:
        raise ConnectTimeout()

    return SocketStream(stream_reader=stream_reader, stream_writer=stream_writer)


class AsyncioUnixSocketBackend(AsyncioBackend):
    def __init__(self, *, uds: str, **kwargs: typing.Any) -> None:
        super().__init__(**kwargs)
        self.uds = uds

    async def open_tcp_stream(
        self,
        hostname: bytes,
        port: int,
        ssl_context: typing.Optional[ssl.SSLContext],
        timeout: TimeoutDict,
    ) -> SocketStream:
        path = self.uds
        return await open_uds_socket_stream(
            path=path, hostname=hostname, timeout=timeout, ssl_context=ssl_context
        )


class AsyncioUnixSocketConnection(AsyncHTTPConnection):
    def __init__(self, *a, uds: str, **kw):
        super().__init__(*a, origin=("http", "localhost", 80), **kw)
        self.backend = AsyncioUnixSocketBackend(uds=uds)

    def request(
        self,
        method: bytes,
        url: "httpcore._types.URL",
        headers: "httpcore._types.Headers" = None,
        stream: "httpcore.base.AsyncByteStream" = None,
        timeout: "httpcore._types.TimeoutDict" = None,
    ):
        self.origin = url_to_origin(url)
        return super().request(method, url, headers, stream, timeout)
